﻿namespace Essentials.StateMachine
{
    public class StatePath
    {
        private StateMachine stateMachine;

        private const string SEPERATOR = " > ";
        
        public StatePath(StateMachine stateMachine)
        {
            this.stateMachine = stateMachine;
        }

        public override string ToString()
        {
            string path = "";
            IState currentState = stateMachine.CurrentState;
            if (currentState == null)
                return path;
            
            while (true)
            {
                path += currentState.ToString();
                StateMachine nextStateMachine = currentState as StateMachine;
                if (nextStateMachine != null && nextStateMachine.CurrentState != null)
                {
                    currentState = nextStateMachine.CurrentState;
                    path += SEPERATOR;
                }
                else
                {
                    break;
                }
            }
            return path;
        }
    }
}