﻿using System;
using System.Collections.Generic;

namespace Essentials.StateMachine
{
    public class StateMachine : IState, IStateEnter, IStateUpdate, IStateExit
    {
        private readonly List<StateEntry> states = new List<StateEntry>();

        private readonly bool moveToNextStateOnFinish;
        private readonly bool removeStateOnExit;
        
        private StateEntry currentState;

        public IState CurrentState
        {
            get
            {
                if (currentState == null)
                    return null;

                return currentState.State;
            }
        }

        public bool IsFinished { get { return StateCount == 0; } }
        
        protected int StateCount { get { return states.Count; } }

        public StateMachine(bool moveToNextStateOnFinish, bool removeStateOnExit)
        {
            this.moveToNextStateOnFinish = moveToNextStateOnFinish;
            this.removeStateOnExit = removeStateOnExit;
        }
        
        public StateMachine(IState state, bool moveToNextStateOnFinish, bool removeStateOnExit)
            : this(moveToNextStateOnFinish, removeStateOnExit)
        {
            AddState(state);
        }
    
        public StateMachine(List<IState> states, bool moveToNextStateOnFinish, bool removeStateOnExit)
            : this(moveToNextStateOnFinish, removeStateOnExit)
        {
            AddStates(states);
        }

        public void Start()
        {
            StateEnter(null);
        }

        public void Stop()
        {
            StateExit();
        }
    
        void IStateEnter.StateEnter(object[] arguments)
        {
            StateEnter(arguments);
        }

        protected virtual void StateEnter(object[] arguments)
        {
            EnterCurrentState(arguments);
        }
    
        void IStateUpdate.StateUpdate()
        {
            StateUpdate();
        }

        protected virtual void StateUpdate()
        {
            Update();
        }
    
        void IStateExit.StateExit()
        {
            StateExit();
        }
        
        protected virtual void StateExit()
        {
            ExitCurrentState();
        }

        public void Update()
        {
            if (currentState == null)
                return;
            
            UpdateCurrentState();
        }
        
        public void MoveToNextState(object[] arguments = null)
        {
            int indexCurrent = states.IndexOf(currentState);
            ExitCurrentState();
            
            if (removeStateOnExit)
            {
                currentState = null;
                states.RemoveAt(indexCurrent);
                TryToSetDefaultCurrentState();
                EnterCurrentState(arguments);
            }
            else
            {
                int nextIndex = GetNextIndex(indexCurrent);
                SetCurrentState(states[nextIndex]);
                EnterCurrentState(arguments);
            }
        }
    
        public void MoveToState(Type type, object[] arguments = null)
        {
            ExitCurrentState();
            SetCurrentState(type);
            EnterCurrentState(arguments);
        }

        public void AddStates(List<IState> states)
        {
            for (int i = 0; i < states.Count; i++)
                AddState(states[i]);
        }
    
        public void AddState(IState state)
        {
            StateEntry stateEntry = new StateEntry(state);

            // Since Initialize is only called once it's checked here instead of StateEntry
            if (IsTypeAssignableFrom<IStateInitialize>(state))
                ((IStateInitialize)state).StateInitialize(this);
            
            states.Add(stateEntry);
        }

        public void RemoveState(IState state)
        {
            StateEntry stateToRemove = GetState(state);
            if (CurrentState == state)
            {
                ExitCurrentState();
                int indexCurrent = states.IndexOf(stateToRemove);
                int indexNext = GetNextIndex(indexCurrent - 1);
                SetCurrentState(states[indexNext]);
                EnterCurrentState(null);
            }

            states.Remove(stateToRemove);
        }
        
        public void TryToSetDefaultCurrentState()
        {
            if (states == null || states.Count == 0)
                return;
    
            SetCurrentState(states[0]);
        }

        protected void SetCurrentState(Type type)
        {
            SetCurrentState(GetStateByType(type));
        }
    
        private void SetCurrentState(StateEntry state)
        {
            currentState = state;
        }
    
        protected void EnterCurrentState(object[] arguments)
        {
            if (currentState == null)
                return;
            
            if (currentState.HasEnter)
                currentState.StateEnter.StateEnter(arguments);
        }
    
        private void UpdateCurrentState()
        {
            if (currentState.HasUpdate)
                currentState.StateUpdate.StateUpdate();

            // It's possible that during an update the state is exited. This check prevents any null references
            if (currentState == null)
                return;
            
            if (currentState.State.IsFinished && moveToNextStateOnFinish)
                MoveToNextState();
        }
    
        private void ExitCurrentState()
        {
            if (currentState == null)
                return;
            
            if (currentState.HasExit)
                currentState.StateExit.StateExit();
        }

        private StateEntry GetState(IState state)
        {
            for (int i = 0; i < states.Count; i++)
                if (states[i].State == state)
                    return states[i];
            return null;
        }
        
        private StateEntry GetStateByType(Type type)
        {
            for (int i = 0; i < states.Count; i++)
                if (states[i].State.GetType() == type)
                    return states[i];
            
            return null;
        }

        private int GetNextIndex(int index)
        {
            int nextIndex = index + 1;
            if (nextIndex >= StateCount)
                nextIndex = 0;
            return nextIndex;
        }

        private bool IsTypeAssignableFrom<T>(IState state)
        {
            var type = state.GetType();
            return typeof(T).IsAssignableFrom(type);
        }
        
        private class StateEntry
        {
            private readonly IState state;
            public IState State { get { return state; } }
            
            private readonly IStateEnter stateEnter;
            public IStateEnter StateEnter { get { return stateEnter; } }
            public bool HasEnter { get; private set; }

            private readonly IStateUpdate stateUpdate;
            public IStateUpdate StateUpdate { get { return stateUpdate; } }
            public bool HasUpdate { get; private set; }

            private readonly IStateExit stateExit;
            public IStateExit StateExit { get { return stateExit; } }
            public bool HasExit { get; private set; }
            
            public StateEntry(IState state)
            {
                this.state = state;
                var type = state.GetType();
                
                HasEnter = typeof(IStateEnter).IsAssignableFrom(type);
                if (HasEnter)
                    stateEnter = (IStateEnter) state;
                
                HasUpdate = typeof(IStateUpdate).IsAssignableFrom(type);
                if (HasUpdate)
                    stateUpdate = (IStateUpdate) state;
                
                HasExit = typeof(IStateExit).IsAssignableFrom(type);
                if (HasExit)
                    stateExit = (IStateExit) state;
            }
        }
    }
}