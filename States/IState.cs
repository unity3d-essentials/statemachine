﻿namespace Essentials.StateMachine
{
    public interface IState
    {
        bool IsFinished { get; }
    }

    public interface IStateInitialize
    {
        void StateInitialize(StateMachine stateMachine);
    }

    public interface IStateEnter
    {
        void StateEnter(object[] arguments);
    }

    public interface IStateUpdate
    {
        void StateUpdate();
    }

    public interface IStateExit
    {
        void StateExit();
    }
}